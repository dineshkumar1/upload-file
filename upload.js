const express = require('express')
const oracledb = require('oracledb');
const app = express();
const port = 3000;
oracledb.autoCommit = true;


app.post('/upload',async(req,res,next)=>{
    try {
        var connection = await oracledb.getConnection({
          user: "cashtrea_testing",
          password: "cashtrea_testing",
          connectString: "ideak6db.chwkrfaqj9m1.ap-south-1.rds.amazonaws.com:1521/ideal6db"
        });
        console.log("Connection has been established!");

        try {
            const maxFileSize = 1024 * 1024 * 50; 
            let contentBuffer = [];
            let totalBytesInBuffer = 0;
            let contentType = req.headers['content-type'] || 'application/octet';
            let fileName = req.headers['x-file-name'];
        
            if (fileName === '') {
              res.status(400).json({error: `The file name must be passed via x-file-name header`});
              return;
            }
        
            req.on('data', chunk => {
              contentBuffer.push(chunk);
              totalBytesInBuffer += chunk.length;
              if (totalBytesInBuffer > maxFileSize) {
                req.pause();
                res.status(413).json({error: `The file size exceeded the limit of ${maxFileSize} bytes`});
                req.connection.destroy();
              }
            });
        
            req.on('end', async function() {
               contentBuffer = Buffer.concat(contentBuffer, totalBytesInBuffer);
              try {
                var result = await connection.execute(`INSERT INTO studentFiles(filename,contenttype,blobdata) VALUES(:filename, :contenttype,:blobdata )`,[fileName,contentType,contentBuffer]);
                if(result){
                     res.status(200).send("File has been successfully uploaded!");
                }
              } catch (err) {
                console.log(err);
                res.status(500).json({error: 'Oops, something broke!'});
                req.connection.destroy();
              }
            });
          } catch (err) {
            res.send(err);
          }
      } catch (err) {
         res.send(err.message);
      } 
})


app.listen(port, () => {
    console.log("App listening on port : ", port)},
  )